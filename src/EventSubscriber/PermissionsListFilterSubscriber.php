<?php

namespace Drupal\permissions_visibility_filter\EventSubscriber;


use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Event\UserEvents;
use Drupal\user\Event\PermissionsListFilterEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * A subscriber for testing PermissionsListFilterEvent.
 */
class PermissionsListFilterSubscriber implements EventSubscriberInterface {

  public function __construct(
    protected AccountInterface $user,
    protected ConfigFactoryInterface $configFactory,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [UserEvents::PERMISSIONS_LIST_FILTER => 'processPermissions'];
  }

  /**
   * Takes a permissions array and removes all keys but a, b, c.
   *
   * @param \Drupal\user\Event\PermissionsListFilterEvent $event
   *   The permissions filter list event.
   */
  public function processPermissions(PermissionsListFilterEvent $event) {
    $permissions = $event->getPermissions();
    $roles = $this->user->getRoles();
    $conditions = $this->configFactory->get('permissions_visibility_filter.settings')->get('conditions');
    // @todo skip user 1?
    foreach ($conditions as $condition) {
      $regex = '/' . str_replace('*', '.*', $condition['permission']) . '/';
      $matches = array_filter(array_keys($permissions), fn($permission) => preg_match($regex, $permission));
      foreach ($matches as $permission_name) {
        if ($condition['show_hide'] === 'hide_unless') {
          // If none of the users roles are in the `roles` condition, hide.
          if (count(array_intersect($condition['roles'], $roles)) === 0) {
            unset($permissions[$permission_name]);
          }
        } else {
          // If one of the users roles is in the `roles` condition, hide.
          if (count(array_intersect($condition['roles'], $roles)) > 0) {
            unset($permissions[$permission_name]);
          }
        }
      }
    }
    $event->setPermissions($permissions);
  }

}

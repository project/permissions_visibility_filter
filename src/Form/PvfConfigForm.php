<?php

namespace Drupal\permissions_visibility_filter\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\user\RoleInterface;


class PvfConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'permissions_visibility_filter_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get all roles.
    $wrapper_id = Html::getUniqueId('visbility-settings-wrapper');
    $roles = Role::loadMultiple();
    unset($roles[RoleInterface::ANONYMOUS_ID]);
    $roles = array_map(fn(RoleInterface $role) => Html::escape($role->label()), $roles);

    if (empty($form_state->get('conditions'))) {
      $config = $this->config('permissions_visibility_filter.settings');
      $stored_conditions = $config->get('conditions') ?? [];
      $form_state->set('conditions', $stored_conditions);
    }
    $conditions = $form_state->get('conditions');

    $form['settings'] = [
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
    ];

    $show_hide_options = [
      'hide_unless' => $this->t('Hide permission unless one of the following roles:'),
      'show_unless' => $this->t('Show permission unless one of the following roles:'),
    ];

    $condition_form = [
      '#type' => 'fieldset',
      'top' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['pvs-group-top'],
        ],
        'show_hide' => [
          '#type' => 'radios',
          '#title' => $this->t('Condition Type'),
          '#options' => $show_hide_options,
          '#default_value' => 'hide_unless',
        ],
        'roles' => [
          '#type' => 'select',
          '#title' => $this->t('Roles'),
          '#options' => $roles,
          '#multiple' => TRUE,
        ],
      ],
      'permission' => [
        '#title' => $this->t('Permission Name'),
        '#type' => 'textfield',
        '#description' => $this->t('Permission name to match. Can use "*" wildcard for matching'),
      ],
    ];

    $form['settings']['new_rule'] = $condition_form + ['add' => [
        '#type' => 'submit',
        '#submit' => ['::updateForm'],
        '#value' => $this->t('Add new condition'),
        '#ajax' => [
          'callback' => '::addRuleAjax',
          'wrapper' => $wrapper_id,
          'effect' => 'fade',
        ],
      ],
    ];
    $form['settings']['new_rule']['#markup'] = '<h2>' . $this->t('Add Condition') . '</h2>';

    foreach ($conditions as $num => $condition ) {
      $rolestring = empty($condition['roles']) ? t('None') : implode(', ', array_keys($condition['roles']));

      if (!empty($condition['edit'])) {
        $form['settings']['existing']["condition_$num"] = array_merge($condition_form, [
          '#markup' => '<h2>' . $this->t('Edit Condition') . '</h2>',
          [
            'save' => [
              '#type' => 'submit',
              '#submit' => ['::updateForm'],
              '#value' => $this->t('Save Changes'),
              '#name' => "save_$num",
              '#op' => "save_$num",
              '#ajax' => [
                'callback' => '::updateItem',
                'wrapper' => $wrapper_id,
                'effect' => 'fade',
              ],
            ],
            'cancel' => [
              '#type' => 'submit',
              '#submit' => ['::updateForm'],
              '#value' => $this->t('Cancel'),
              '#name' => "cancel_$num",
              '#op' => "cancel_$num",
              '#ajax' => [
                'callback' => '::updateItem',
                'wrapper' => $wrapper_id,
                'effect' => 'fade',
              ],
            ],
          ]
        ]);
        $form['settings']['existing']["condition_$num"]['top']['show_hide']['#value'] = $condition['show_hide'];
        $form['settings']['existing']["condition_$num"]['top']['roles']['#value'] = $condition['roles'];
        $form['settings']['existing']["condition_$num"]['permission']['#value'] = $condition['permission'];
        $form['settings']['existing']["condition_$num"]['top']['show_hide_update'] = $form['settings']['existing']["condition_$num"]['top']['show_hide'];
        unset($form['settings']['existing']["condition_$num"]['top']['show_hide']);
        $form['settings']['existing']["condition_$num"]['top']['roles_update'] = $form['settings']['existing']["condition_$num"]['top']['roles'];
        unset($form['settings']['existing']["condition_$num"]['top']['roles']);
        $form['settings']['existing']["condition_$num"]['permission_update'] = $form['settings']['existing']["condition_$num"]['permission'];
        unset($form['settings']['existing']["condition_$num"]['permission']);
      } else {
        $form['settings']['existing']["condition_$num"] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['condition-item'],
          ],
          '#markup' => "<strong>{$condition['permission']}</strong>, <i>({$condition['show_hide']})</i>: $rolestring",
          'edit' => [
            '#type' => 'submit',
            '#submit' => ['::updateItem'],
            '#value' => t('Edit'),
            '#name' => "edit_$num",
            '#op' => "edit_$num",
            '#ajax' => [
              'op' => "edit_$num",
              'callback' =>  '::addRuleAjax',
              'wrapper' => $wrapper_id,
              'effect' => 'fade',
            ],
          ],
          'delete' => [
            '#type' => 'submit',
            '#value' => t('Delete'),
            '#name' => "remove_$num",
            '#submit' => ['::updateItem'],
            '#op' => "remove_$num",
            '#ajax' => [
              'callback' =>  '::addRuleAjax',
              'wrapper' => $wrapper_id,
              'effect' => 'fade',
            ],
          ]
        ];
      }
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save Conditions'),
    ];

    $form['#attached']['library'][] = 'permissions_visibility_filter/permissions_visibility_filter';
    return $form;
  }

  public function updateForm($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $conditions = $form_state->get('conditions');
    $conditions[]  = [
      'permission' => $values['permission'],
      'roles' => $values['roles'],
      'show_hide' => $values['show_hide'],
    ];
    $form_state->set('conditions', $conditions);
    $form_state->setRebuild();
  }
  public function addRuleAjax(array $form, FormStateInterface $form_state)  {
    $form["settings"]["new_rule"]["permission"]["#value"] = '';
    $form["settings"]["new_rule"]["top"]["roles"]["#value"] = [];
    $form["settings"]["new_rule"]["top"]["show_hide"]["#value"] = 'hide_unless';
    return $form['settings'];
  }

  public function updateItem($form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    [$op, $id] = explode('_', $trigger['#op']);
    $conditions = $form_state->get('conditions');
    switch ($op) {
      case 'remove':
        unset($conditions[$id]);
        break;
      case 'edit':
        $conditions[$id]['edit'] = TRUE;
        break;
      case 'cancel':
        unset( $conditions[$id]['edit'] );
        break;
      case 'save':
        unset( $conditions[$id]['edit'] );
        $input = $form_state->getUserInput();
        $conditions[$id]['show_hide'] = $input['show_hide_update'];
        $conditions[$id]['roles'] = $input['roles_update'];
        $conditions[$id]['permission'] = $input['permission_update'];
        break;
    }
    $form_state->set('conditions', array_values($conditions));
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('permissions_visibility_filter.settings');
    $conditions = $form_state->get('conditions');
    $config->set('conditions', $conditions);
    $config->save();

    $this->messenger()->addMessage($this->t('Configuration has been saved.'));
  }

  protected function getEditableConfigNames() {
    return ['permissions_visibility_filter.settings'];
  }

}
